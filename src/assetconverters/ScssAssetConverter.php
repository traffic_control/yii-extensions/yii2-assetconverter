<?php

namespace credy\assetconverter\assetconverters;

use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\OutputStyle;
use yii\web\AssetConverterInterface;

class ScssAssetConverter extends FileWriterAssetConverter implements AssetConverterInterface
{
    /**
     * Whether to force converting or not if compiled asset already exists
     *
     * @var boolean
     */
    public $forceConvert = false;

    /**
     * If output should be compressed or not
     *
     * @see https://scssphp.github.io/scssphp/docs/#output-formatting
     *
     * @var string
     */
    public $outputStyle = OutputStyle::EXPANDED;

    public $sourceMap = Compiler::SOURCE_MAP_NONE;

    public function convert($asset, $basePath)
    {
        $result = substr($asset, 0, -5) . '.css';

        if (!$this->forceConvert && $this->fileExists($basePath . '/' . $result)) {
            return $result;
        }
        $compiler = $this->getCompiler();

        $compiler->addImportPath(dirname($basePath . '/' . $asset));
        $compiler->setOutputStyle($this->outputStyle);
        $compiler->setSourceMap($this->sourceMap);

        $compiler->setSourceMapOptions([
            'sourceMapURL' => $result . '.map',
            'sourceMapFilename' => $result,
            'sourceMapBasepath' => $basePath
        ]);

        $scssString = $this->readFile($basePath . '/' . $asset);

        $compiledString = $compiler->compileString($scssString);

        $this->writeFile($basePath . '/' . $result, $compiledString->getCss());

        if ($compiledString->getSourceMap() && $this->sourceMap === Compiler::SOURCE_MAP_FILE) {
            $this->writeFile($basePath . '/' . $result . '.map', $compiledString->getSourceMap());
        }

        return $result;
    }

    /**
     * @return Compiler
     */
    protected function getCompiler()
    {
        return new Compiler();
    }
}
