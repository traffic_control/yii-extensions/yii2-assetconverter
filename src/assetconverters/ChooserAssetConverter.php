<?php

namespace credy\assetconverter\assetconverters;

use yii\base\Component;
use yii\di\Instance;
use yii\web\AssetConverter;
use yii\web\AssetConverterInterface;

class ChooserAssetConverter extends Component implements AssetConverterInterface
{
    public $assetConverters = [
        'less' => [
            'class' => AssetConverter::class,
            'commands' => [
                'less' => ['css', 'lessc {from} {to} --no-color --source-map'],
            ]
        ],
        'scss' => [
            'class' => ScssAssetConverter::class,
        ],
        'sass' => [
            'class' => AssetConverter::class,
            'commands' => [
                'sass' => ['css', 'sass {from} {to} --sourcemap'],
            ]
        ],
        'styl' => [
            'class' => AssetConverter::class,
            'commands' => [
                'styl' => ['css', 'stylus < {from} > {to}'],
            ]
        ],
        'coffee' => [
            'class' => AssetConverter::class,
            'commands' => [
                'coffee' => ['js', 'coffee -p {from} > {to}'],
            ]
        ],
        'ts' => [
            'class' => AssetConverter::class,
            'commands' => [
                'ts' => ['js', 'tsc --out {to} {from}'],
            ]
        ],
    ];

    public function convert($asset, $basePath)
    {
        $pos = strrpos($asset, '.');
        if ($pos !== false) {
            $ext = substr($asset, $pos + 1);
            if (isset($this->assetConverters[$ext])) {
                /**
                 * @var AssetConverterInterface $assetConverter
                 */
                $assetConverter = Instance::ensure($this->assetConverters[$ext], AssetConverterInterface::class);
                return $assetConverter->convert($asset, $basePath);
            }
        }
        return $asset;
    }
}
