<?php

namespace credy\assetconverter\assetconverters;

use yii\base\Component;
use yii\web\AssetConverterInterface;

abstract class FileWriterAssetConverter extends Component implements AssetConverterInterface
{
    /**
     * @param string $path
     * @param string $contents
     *
     * @return int|false
     */
    protected function writeFile($path, $contents)
    {
        return file_put_contents($path, $contents);
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function readFile($path)
    {
        return file_get_contents($path);
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    protected function fileExists($path)
    {
        return file_exists($path);
    }
}
