<?php

namespace credy\assetconverter\controllers;

use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use Yii;
use yii\console\controllers\AssetController as ControllersAssetController;
use yii\console\Exception;

class AssetController extends ControllersAssetController
{
    /**
     * Compresses given JavaScript files and combines them into the single one.
     *
     * @param array $inputFiles list of source file names.
     * @param string $outputFile output file name.
     *
     * @throws \yii\console\Exception on failure
     */
    protected function compressJsFiles($inputFiles, $outputFile)
    {
        if (empty($inputFiles)) {
            return;
        }
        $this->stdout("  Compressing JavaScript files...\n");

        $minifier = $this->getJsCompressor();

        foreach ($inputFiles as $inputFile) {
            $minifier->add($inputFile);
        }

        $minifier->minify($outputFile);

        if (!file_exists($outputFile)) {
            throw new Exception("Unable to compress JavaScript files into '{$outputFile}'.");
        }
        $this->stdout("  JavaScript files compressed into '{$outputFile}'.\n");
    }

    /**
     * Compresses given CSS files and combines them into the single one.
     *
     * @param array $inputFiles list of source file names.
     * @param string $outputFile output file name.
     *
     * @throws \yii\console\Exception on failure
     */
    protected function compressCssFiles($inputFiles, $outputFile)
    {
        if (empty($inputFiles)) {
            return;
        }
        $this->stdout("  Compressing CSS files...\n");

        $minifier = $this->getCssCompressor();

        foreach ($inputFiles as $inputFile) {
            $minifier->add($inputFile);
        }

        $minifier->minify($outputFile);

        if (!file_exists($outputFile)) {
            throw new Exception("Unable to compress CSS files into '{$outputFile}'.");
        }
        $this->stdout("  CSS files compressed into '{$outputFile}'.\n");
    }

    /**
     * @return CSS
     */
    protected function getCssCompressor()
    {
        return new CSS();
    }

    /**
     * @return JS
     */
    protected function getJsCompressor()
    {
        return new JS();
    }
}
