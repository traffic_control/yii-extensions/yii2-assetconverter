<?php

namespace tests\unit\assetconverters;

use Codeception\Test\Unit;

class FileWriterAssetConverterTest extends Unit
{
    public function testWriteFile()
    {
        /**
         * @var FileWriterAssetConverter
         */
        $fileWriter = $this->make(FileWriterAssetConverter::class);

        $tempName = tempnam(sys_get_temp_dir(), 'test');

        $this->assertEquals(4, $fileWriter->writeFile($tempName, 'tere'));
        $this->assertFileExists($tempName);
    }

    public function testReadFile()
    {
        /**
         * @var FileWriterAssetConverter
         */
        $fileWriter = $this->make(FileWriterAssetConverter::class);

        $this->assertEquals('test', $fileWriter->readFile(codecept_data_dir() . '/test.txt'));
    }

    public function testFileExists()
    {
        /**
         * @var FileWriterAssetConverter
         */
        $fileWriter = $this->make(FileWriterAssetConverter::class);

        $this->assertTrue($fileWriter->fileExists(codecept_data_dir() . '/test.txt'));
    }

    public function testFileExistsFalseOnFileNotFound()
    {
        /**
         * @var FileWriterAssetConverter
         */
        $fileWriter = $this->make(FileWriterAssetConverter::class);

        $this->assertFalse($fileWriter->fileExists(codecept_data_dir() . '/not_found.txt'));
    }
}
