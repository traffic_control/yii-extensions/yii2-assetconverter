<?php

namespace tests\unit\assetconverters;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use credy\assetconverter\assetconverters\ScssAssetConverter;
use ScssPhp\ScssPhp\CompilationResult;
use ScssPhp\ScssPhp\Compiler;
use ScssPhp\ScssPhp\OutputStyle;

class ScssAssetConverterTest extends Unit
{
    public function testFileIsGeneratedWhenFileNotFound()
    {
        /**
         * @var ScssAssetConverter
         */
        $converter = $this->construct(ScssAssetConverter::class, [], [
            'writeFile' => Expected::once(),
            'fileExists' => Expected::once(false),
        ]);

        $converter->convert('test.scss', codecept_data_dir('scss'));
    }

    public function testFileIsGeneratedWhenFileIsFound()
    {
        /**
         * @var ScssAssetConverter
         */
        $converter = $this->construct(ScssAssetConverter::class, [], [
            'writeFile' => Expected::never(),
            'fileExists' => Expected::once(true),
        ]);

        $converter->convert('test.scss', codecept_data_dir('scss'));
    }

    public function testFileIsGeneratedWhenForceConvertIsOn()
    {
        /**
         * @var ScssAssetConverter
         */
        $converter = $this->construct(ScssAssetConverter::class, [[
            'forceConvert' => true,
        ]], [
            'writeFile' => Expected::once(),
            'fileExists' => Expected::never(),
        ]);

        $converter->convert('test.scss', codecept_data_dir('scss'));
    }


    public function testAddImportPathIsCorrect()
    {
        /**
         * @var ScssAssetConverter
         */
        $converter = $this->construct(ScssAssetConverter::class, [], [
            'writeFile' => Expected::once(),
            'fileExists' => Expected::once(false),
            'getCompiler' => function () {
                return $this->makeEmpty(Compiler::class, [
                    'addImportPath' => Expected::once(function ($importPath) {
                        $this->assertEquals(codecept_data_dir('scss'), $importPath);
                    }),
                    'compileString' => new CompilationResult('', '', [])
                ]);
            }
        ]);

        $converter->convert('test.scss', codecept_data_dir('scss'));
    }

    public function testOutputStyleIsGivenToCompiler()
    {
        /**
         * @var ScssAssetConverter
         */
        $converter = $this->construct(ScssAssetConverter::class, [[
            'outputStyle' => OutputStyle::COMPRESSED
        ]], [
            'writeFile' => Expected::once(),
            'fileExists' => Expected::once(false),
            'getCompiler' => function () {
                return $this->makeEmpty(Compiler::class, [
                    'setOutputStyle' => Expected::once(function ($outputStyle) {
                        $this->assertEquals(OutputStyle::COMPRESSED, $outputStyle);
                    }),
                    'compileString' => new CompilationResult('', '', [])
                ]);
            }
        ]);

        $converter->convert('test.scss', codecept_data_dir('scss'));
    }

    public function testSourceMapIsGivenToCompiler()
    {
        /**
         * @var ScssAssetConverter
         */
        $converter = $this->construct(ScssAssetConverter::class, [[
            'sourceMap' => Compiler::SOURCE_MAP_INLINE
        ]], [
            'writeFile' => Expected::once(),
            'fileExists' => Expected::once(false),
            'getCompiler' => function () {
                return $this->makeEmpty(Compiler::class, [
                    'setSourceMap' => Expected::once(function ($sourceMap) {
                        $this->assertEquals(Compiler::SOURCE_MAP_INLINE, $sourceMap);
                    }),
                    'compileString' => new CompilationResult('', '', [])
                ]);
            }
        ]);

        $converter->convert('test.scss', codecept_data_dir('scss'));
    }

    public function testIfSourceFileIsSetToFileSourceFileIsWritten()
    {
        /**
         * @var ScssAssetConverter
         */
        $converter = $this->construct(ScssAssetConverter::class, [[
            'sourceMap' => Compiler::SOURCE_MAP_FILE
        ]], [
            'writeFile' => Expected::exactly(2),
            'fileExists' => Expected::once(false),
            'getCompiler' => function () {
                return $this->makeEmpty(Compiler::class, [
                    'setSourceMap' => Expected::once(function ($sourceMap) {
                        $this->assertEquals(Compiler::SOURCE_MAP_FILE, $sourceMap);
                    }),
                    'compileString' => new CompilationResult('', 'sourcefile', [])
                ]);
            }
        ]);

        $converter->convert('test.scss', codecept_data_dir('scss'));
    }
}
