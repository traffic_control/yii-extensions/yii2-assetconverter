<?php

namespace tests\unit\assetconverters;

abstract class FileWriterAssetConverter extends \credy\assetconverter\assetconverters\FileWriterAssetConverter
{
    public function writeFile($path, $contents)
    {
        return parent::writeFile($path, $contents);
    }

    public function readFile($path)
    {
        return parent::readFile($path);
    }

    public function fileExists($path)
    {
        return parent::fileExists($path);
    }
}
