<?php

namespace tests\unit\assetconverters;

use Codeception\Test\Unit;
use credy\assetconverter\assetconverters\ChooserAssetConverter;
use yii\web\AssetConverter;

class ChooserAssetConverterTest extends Unit
{
    public function testConverterFoundForAsset()
    {
        $converter = new ChooserAssetConverter([
            'assetConverters' => [
                'scss' => $this->make(AssetConverter::class, [
                    'convert' => 'converted.css'
                ])
            ]
        ]);

        $this->assertEquals('converted.css', $converter->convert('test.scss', codecept_data_dir('scss')));
    }

    public function testConverterNotFoundForAsset()
    {
        $converter = new ChooserAssetConverter([
            'assetConverters' => [
            ]
        ]);

        $this->assertEquals('test.scss', $converter->convert('test.scss', codecept_data_dir('scss')));
    }
}
