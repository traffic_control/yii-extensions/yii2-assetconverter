<?php

namespace tests\unit\controllers;

use Codeception\Stub\Expected;
use Codeception\Test\Unit;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use yii\console\Exception;

class AssetControllerTest extends Unit
{
    public function testCompressJsFilesOnEmptyInputFiles()
    {
        /**
         * @var AssetController
         */
        $controller = $this->make(AssetController::class, [
            'stdout' => Expected::never()
        ]);

        $controller->compressJsFiles([], 'test');
    }

    public function testCompressJsFilesWithInputFiles()
    {
        $tempName = tempnam(sys_get_temp_dir(), 'test');

        /**
         * @var AssetController
         */
        $controller = $this->make(AssetController::class, [
            'stdout' => Expected::exactly(2),
            'getJsCompressor' => $this->make(JS::class, [
                'add' => Expected::once(),
                'minify' => Expected::once(function ($outputFile) use ($tempName) {
                    $this->assertEquals($tempName, $outputFile);
                })
            ])
        ]);

        $controller->compressJsFiles([
            codecept_data_dir('js/test.js')
        ], $tempName);
    }

    public function testCompressJsFilesWithInputFilesFails()
    {
        /**
         * @var AssetController
         */
        $controller = $this->make(AssetController::class, [
            'stdout' => Expected::once(),
            'getJsCompressor' => $this->make(JS::class, [
                'add' => Expected::once(),
                'minify' => Expected::once()
            ])
        ]);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('nable to compress JavaScript files into \'test.js\'.');

        $controller->compressJsFiles([
            codecept_data_dir('js/test.js')
        ], 'test.js');
    }

    public function testCompressCssFilesOnEmptyInputFiles()
    {
        /**
         * @var AssetController
         */
        $controller = $this->make(AssetController::class, [
            'stdout' => Expected::never()
        ]);

        $controller->compressCssFiles([], 'test');
    }

    public function testCompressCssFilesWithInputFiles()
    {
        $tempName = tempnam(sys_get_temp_dir(), 'test');

        /**
         * @var AssetController
         */
        $controller = $this->make(AssetController::class, [
            'stdout' => Expected::exactly(2),
            'getCssCompressor' => $this->make(CSS::class, [
                'add' => Expected::once(),
                'minify' => Expected::once(function ($outputFile) use ($tempName) {
                    $this->assertEquals($tempName, $outputFile);
                })
            ])
        ]);

        $controller->compressCssFiles([
            codecept_data_dir('css/test.css')
        ], $tempName);
    }

    public function testCompressCssFilesWithInputFilesFails()
    {
        /**
         * @var AssetController
         */
        $controller = $this->make(AssetController::class, [
            'stdout' => Expected::once(),
            'getCssCompressor' => $this->make(CSS::class, [
                'add' => Expected::once(),
                'minify' => Expected::once()
            ])
        ]);

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Unable to compress CSS files into \'test.css\'.');

        $controller->compressCssFiles([
            codecept_data_dir('css/test.css')
        ], 'test.css');
    }
}
