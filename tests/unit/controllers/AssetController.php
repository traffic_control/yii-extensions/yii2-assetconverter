<?php

namespace tests\unit\controllers;

class AssetController extends \credy\assetconverter\controllers\AssetController
{
    public function compressJsFiles($inputFiles, $outputFile)
    {
        return parent::compressJsFiles($inputFiles, $outputFile);
    }

    public function compressCssFiles($inputFiles, $outputFile)
    {
        return parent::compressCssFiles($inputFiles, $outputFile);
    }
}
