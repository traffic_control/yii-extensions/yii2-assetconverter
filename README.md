# Yii2 module for asset conversion and asset compression


## Installation:

The preferred way to install this extension is through [composer](https://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist credy/yii2-assetconverter "^1.0"
```

or add

```
"credy/yii2-assetconverter": "^1.0"
```

to the require section of your composer.json.


## Configuration:
### AssetController:

put following snippet into your application config

```php
[
    'controllerMap' => [
        'assets' => [
            'class' => credy\assetconverter\controllers\AssetController::class
        ],
    ],
]
```
#### Usage:
You can use it exactly as Yii2 [AssetController](https://www.yiiframework.com/doc/api/2.0/yii-console-controllers-assetcontroller)

### AssetConverters:
Put following snippet into your application config

```php
[
    'components' => [
        'assetManager' => [
            'converter' => [
                'class' => credy\assetconverter\assetconverters\ChooserAssetConverter::class
            ]
        ]
    ],
];
```
